'use strict';
const config = require('../config');
const appServer = require('./web-server');
// const KnexSessionStore = require('connect-session-knex')(session);

// const knex = require('knex')({
//   client: 'pg',
//   connection: config.pgconnection,
//   version: '10',
// });

// const store = new KnexSessionStore({ knex });

const { router, app, isDev, isProd, env } = appServer({
  session: {
    secret: config.session.secret,
    //store,
  },
});

app.set('view engine', 'pug');

router.get('/', async (req, res) => {
  if(req.session.saidHi) {
    res.json({ message: 'Thanks for coming back!' });
  } else {
    req.session.saidHi = true;
    res.json({ message: 'Hello, stranger!' });
  }
});

router.get('/error', async (req, res) => {
  return Promise.reject(new Error('This be an errar'));
});

app.use((req, res) => {
  res.sendStatus(404);
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.status(500);
  if (req.accepts('html')) {
    res.send('Something went wrong. Please refresh and try again.');
  } else {
    res.json({
      error: 'Something went wrong processing your request, please try again.',
    });
  }
  throw err;
});

// If securePort is defined in config, then we want to enable serving
// over https on that port. Otherwise, the app is probably being served
// from behind a proxy which terminates tls.
app.easySelfSignedHttpsListen(config.securePort, () => {
  console.log(`listening at https://localhost:${config.securePort}.`);
});

app.listen(config.port, () => {
  console.log(`listening at http://localhost:${config.port}.`);
});
