module.exports.noSessionConfig =
`
=- App Server message to you! -=
session configuration is required.
At minimum, you must pass a \`secret\` property in the session options to use the in-memory development store.

That can just be:

const app = appServer({ session: { secret: <your long string secret> } });

You can generate a secret by running \`require('crypto').randomBytes(48).toString('hex');\` in a node command line.

This value should be saved in your config.js file which is ignored by git.

More info on generating secrets and sessions: https://@TODO

=- message terminated -=
`;

module.exports.noSessionSecret =
`
=- App Server message to you! -=
session.secret is required in appServer config.

That can just be:

{
  session: {
    secret: <your long string secret>
  }
}

It must be at least 10 characters long, and preferably at least 32 bytes.

You can generate a secret by running \`require('crypto').randomBytes(48).toString('hex');\` in a node command line.

This value should be saved in your config.js file which is ignored by git.

More info on generating secrets and sessions: https://@TODO

=- message terminated -=
`;

module.exports.usingInMemoryStore =
`
=- App Server message to you! -=
Using default in-memory store for sessions.
All user sessions will be lost on server restart.

You should add a \`store\` option to your session configuration, like this:

cosnt app = appServer({
  //...
  session: {
    store: new KnexSessionStore({ knex }),
  },
});

You need a database setup for the sessions to be stored in, and knex configured.
How to setup sessions and knex, and more information: https://@TODO

=- message terminated -=
`;
