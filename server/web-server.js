'use strict';
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const csrf = require('csurf');
const messages = require('./web-server-errors');

function makeSessionConfig (config) {

  if (config.session == null) {
    throw new Error(messages.noSessionConfig);
  }

  if (config.session === false) {
    // Explicitly disabled sessions
    return false;
  }

  const sessionConfig = {
    httpOnly: true,
    secure: true,
    resave: false,
    saveUninitialized: false,
    ...config.session,
  };

  if (typeof config.session.secret !== 'string' || config.session.secret.length < 10) {
    throw new Error(messages.noSessionSecret);
  }

  if (!config.session.store) {
    console.warn(messages.usingInMemoryStore);
  }

  return sessionConfig;
}

module.exports = function (config) {

  const sessionConfig = makeSessionConfig(config);

  const app = express();
  const env = app.get('env');
  const isDev = env === 'development';
  const isProd = env === 'production';

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(session(sessionConfig));
  app.use(csrf({
    value: req => req.get('csrf-token'),
  }));

  app.easySelfSignedHttpsListen = function (...args) {
    const generate = require('self-signed');
    const pems = generate({
      name: 'localhost',
      city: 'New York',
      state: 'New York',
      organization: 'GenericAppServer',
      unit: 'GenericAppServer'
    }); // expires in 1 year
    const httpsServer = https.createServer(
      {
        key: pems.private,
        cert: pems.cert,
      },
      app
    );
    httpsServer.listen(...args);
    return httpsServer;
  };

  const router = require('express-promise-router')();
  app.use(router);

  return {
    router,
    app,
    env,
    isDev,
    isProd,
  };
};
